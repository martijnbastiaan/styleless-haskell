{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}

module Plugin.StylelessHaskell where

import GhcPlugins

import Plugin.StylelessHaskell.Checks.Alphabetization (warnNonAlphabetical)
import Plugin.StylelessHaskell.Checks.Alignment (warnOnAlignment)
import Plugin.StylelessHaskell.Checks.PackageImports (warnNonPackageImport)

plugin :: Plugin
plugin = defaultPlugin
  { parsedResultAction = pluginMain
  , pluginRecompile = \_ -> pure NoForceRecompile
  }

pluginMain ::
  [CommandLineOption] ->
  ModSummary ->
  HsParsedModule ->
  Hsc HsParsedModule
pluginMain _args _modSummary parsedModule =
      pure parsedModule
  >>= warnOnAlignment
  >>= warnNonAlphabetical
  >>= warnNonPackageImport
