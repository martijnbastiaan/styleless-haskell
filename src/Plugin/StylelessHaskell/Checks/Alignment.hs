module Plugin.StylelessHaskell.Checks.Alignment where

import GhcPlugins

warnOnAlignment :: HsParsedModule -> Hsc HsParsedModule
warnOnAlignment parsedModule = pure parsedModule
