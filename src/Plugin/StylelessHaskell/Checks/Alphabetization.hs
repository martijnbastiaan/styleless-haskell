module Plugin.StylelessHaskell.Checks.Alphabetization where

import Data.Maybe (mapMaybe)

import Bag
import ErrUtils
import GHC
import GhcPlugins

import Plugin.StylelessHaskell.Common (getImportGroups)

warnNonAlphabetical :: HsParsedModule -> Hsc HsParsedModule
warnNonAlphabetical parsedModule = Hsc $ \env warnings0 -> do
  let
    dynFlags = hsc_dflags env
    importGroups = getImportGroups parsedModule
    warnings1 = mapMaybe (warnNonAlphabetical1 dynFlags) importGroups

  pure (parsedModule, warnings0 `unionBags` listToBag warnings1)

warnNonAlphabetical1 :: DynFlags -> [LImportDecl GhcPs] -> Maybe WarnMsg
warnNonAlphabetical1 _ _ = Nothing
