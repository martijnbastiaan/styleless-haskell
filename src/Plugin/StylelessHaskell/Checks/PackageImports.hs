{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}

module Plugin.StylelessHaskell.Checks.PackageImports where

-- base
import Data.Maybe (mapMaybe)

-- ghc
import Bag (unionBags, listToBag)
import ErrUtils (WarnMsg, mkPlainWarnMsg)
import GHC (GhcPs, ImportDecl(ImportDecl, ideclName, ideclPkgQual), LImportDecl)
import GhcPlugins
  ( HsParsedModule, Hsc(..), HscEnv(hsc_dflags), GenLocated(L), elementOfUniqSet
  , mkUniqSet )

-- me!
import Plugin.StylelessHaskell.Common

-- | Warn if (non-home) module is imported without specifying a package
warnNonPackageImport :: HsParsedModule -> Hsc HsParsedModule
warnNonPackageImport parsedModule = Hsc $ \env warnings0 -> do
  let
    homeModules = mkUniqSet (getHomeModules env)
    warnings1 = mapMaybe go (getImports parsedModule)
    warning = "Use explicit package imports for external packages"

    go :: LImportDecl GhcPs -> Maybe WarnMsg
    go (L loc (ImportDecl{ideclName=L _ modName, ideclPkgQual}))
      | modName `elementOfUniqSet` homeModules = Nothing
      | Just _ <- ideclPkgQual = Nothing
      | otherwise = Just (mkPlainWarnMsg (hsc_dflags env) loc warning)
    go _ = Nothing

  pure (parsedModule, warnings0 `unionBags` listToBag warnings1)
