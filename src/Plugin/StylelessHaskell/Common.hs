{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE ViewPatterns #-}

module Plugin.StylelessHaskell.Common where

import GhcPlugins
import GHC

-- | Extract imports from a parsed module, group by newline separation
getImportGroups :: HsParsedModule -> [[LImportDecl GhcPs]]
getImportGroups =  go (-1) [] . getImports
 where
  go prevLine curGroup = \case
    [] -> [curGroup]
    (i@(L srcSpan _):is) ->
      case srcSpan of
        RealSrcSpan loc
          | srcSpanStartLine loc == prevLine + 1 ->
              go (srcSpanEndLine loc) (i:curGroup) is
          | otherwise ->
              reverse curGroup : go (srcSpanEndLine loc) [i] is
        UnhelpfulSpan _ ->
          reverse curGroup : [i] : go (-1) [] is

-- | Extract imports from parsed module
getImports :: HsParsedModule -> [LImportDecl GhcPs]
getImports (HsParsedModule{hpm_module=L _ HsModule{hsmodImports}}) = hsmodImports

-- | Extract all module names from the mod graph in the given environment
getHomeModules :: HscEnv -> [ModuleName]
getHomeModules (HscEnv{hsc_mod_graph}) =
  map getModuleName (mgModSummaries hsc_mod_graph)

-- | Extract module name from module summary
getModuleName :: ModSummary -> ModuleName
getModuleName ModSummary{ms_mod=Module _unitId modName} = modName
